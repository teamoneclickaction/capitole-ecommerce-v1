package com.capitole.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapitoleEcommerceApplication {
    public static void main(String[] args) {
        SpringApplication.run(CapitoleEcommerceApplication.class, args);
    }

}



